# general
alias c='clear'
alias e='exit'
alias cl='c;l'
alias t='tree'
alias h='history'
alias sagi='sudo apt-get install'
alias cd..='cd ..'
alias ..='cd ..'
alias rm='rm -I --preserve-root'
alias mv='mv -i'
alias cp='cp -i'
alias ln='ln -i'
alias sshno='ssh -o PubkeyAuthentication=no'
alias flipcaps="python -c 'from ctypes import *; X11 = cdll.LoadLibrary(\"libX11.so.6\"); display = X11.XOpenDisplay(None); X11.XkbLockModifiers(display, c_uint(0x0100), c_uint(2), c_uint(0)); X11.XCloseDisplay(display)'"
alias pw='pwgen 32 1 --capitalize --numerals --symbols  --ambiguous'
alias weather='curl wttr.in/nyc'
alias fix='echo -e "\033c"'
# alias ff='find . -type f -name '
alias ff='fd'
alias nos='nosetests --nologcapture '
alias ns='nosetests --nologcapture --logging-level=INFO '
# https://unix.stackexchange.com/a/4176
alias filecount='find -maxdepth 1 -type d | while read -r dir; do printf "%s:\t" "$dir"; find "$dir" -type f | wc -l; done'
alias jump='xfreerdp -z --disable-wallpaper -a 32 -u username -d domain.net rds-hostname'

alias v='nvim'
alias vr='nvim -R'

# https://stackoverflow.com/a/36925113
alias checkciphers="openssl ciphers -v | awk '{print $2}' | sort | uniq"

alias psqlog="psql"
alias psql="PAGER='/usr/local/bin/nvim -R -u ~/.vimrcpg -' /usr/pgsql-11/bin/psql"
alias pgcli="PAGER='/usr/local/bin/nvim -R -u ~/.vimrcpg -' pgcli"
alias sm='SLACK_TOKEN=TOKEN slackcli -u obed -h HHH -m'
alias gti='git'

alias mongo-uat='mongo host:9011/dbname -u username -p --ssl'
alias mongo-prod='mongo host:9011/dbname -u username -p --ssl'
alias mongo-qa='mongo host:9011/dbname -u username -p --ssl'
alias mongo-vagr='mongo host:9011/dbname -u username -p'

# alias lock='~/Utilities/i3lock-fancy/lock -f Hack-Regular -p'
# alias lock='i3lock -i ~/Downloads/Windows_10_lock_screen_hero.png -c 000000 -p win'
alias lock='i3lock -c 000000'

# locations
alias ansi='cd ~/Source/ansible'

# processes
alias workers="ps aux | grep 'kai_server -w' | awk '{print \$2\" \"\$11\" \"\$12\" \"\$13}'"
alias killworkers="kill \$(ps aux | grep '[k]ai_server -w' | awk '{print \$2}')"
alias psgrep="ps aux | rg"
alias psvgrep="ps aux | grep -v grep | grep"

# tmux
alias tat='tmux attach -t'
alias tns='tmux new-session -s'
alias tls='tmux list-sessions'

# development
alias buildw='./../../venv_exec ./build.sh -w'
alias builds='./../../venv_exec ./build.sh -s'
alias silent='builds > /dev/null 2>&1'
alias flush="(echo 'flush_all'; echo "quit") | nc localhost 11211"
alias superdown='./venv_exec bin/supervisorctl -c conf/supervisord.conf shutdown'
alias gitlog="git log --graph --pretty=format:'%Cred%h%Creset -%C(yellow)%d%Creset %s %Cgreen(%cr) %C(bold blue)<%an>%Creset' --abbrev-commit --date=relative"
alias branch="git status | head -n 1 | cut -d' ' -f4-"
alias builddoc="cd enso_repository/enso/doc && ../../../bin/sphinx-apidoc --force --no-toc --module-first --maxdepth 3 --output-dir doc ../src/kai/ && make html SPHINXBUILD=../../../bin/sphinx-build && cd -"
alias s="source ../../bin/activate"
alias infrapull='find $repos/../ -mindepth 2 -maxdepth 2 | tail -n +2 | while read line; do cd $line && echo $PWD && git status && git pull origin `git rev-parse --abbrev-ref HEAD` ; cd $repos ; done'
alias uat='echo "UAT_$(date -d "this Thursday" "+%Y%m%d")"'
alias wincrypt='cat ~/Notes/wincrypt.txt | xclip -selection c'
alias winconnect='xfreerdp -z --disable-wallpaper -a 32 -u username -g 800x600 -p $(cat ~/Notes/wincrypt.txt) -d domain '

# programs
alias bc='bc --quiet'
alias wireshark='sudo ~/Downloads/wireshark-2.0.2/wireshark-gtk'

# utilities
# alias diff=colordiff
alias mount='mount |column -t'
alias now='date +%T'
alias nowtime=now
alias nowdate='date +"%d-%m-%Y"'
alias day='date +%A'
alias ports='netstat -tulanp'

## forgot where I copied these from:

## pass options to free ##
alias meminfo='free -m -l -t'

## get top process eating memory
alias psmem='ps auxf | sort -nr -k 4'
alias psmem10='ps auxf | sort -nr -k 4 | head -10'

## get top process eating cpu ##
alias pscpu='ps auxf | sort -nr -k 3'
alias pscpu10='ps auxf | sort -nr -k 3 | head -10'

## Get server cpu info ##
alias cpuinfo='lscpu'

## get GPU ram on desktop / laptop##
alias gpumeminfo='grep -i --color memory /var/log/Xorg.0.log'

# reboot / halt / poweroff
alias reboot='sudo /sbin/reboot'
alias poweroff='sudo /sbin/poweroff'
alias halt='sudo /sbin/halt'
alias shutdown='sudo /sbin/shutdown'

# distro specific  - Debian / Ubuntu and friends #
# install with apt-get
alias apt-get="sudo apt-get"
alias updatey="sudo apt-get --yes"

# update on one command
alias update='sudo apt-get update && sudo apt-get upgrade'

# Parenting changing perms on / #
alias chown='chown --preserve-root'
alias chmod='chmod --preserve-root'
alias chgrp='chgrp --preserve-root'

alias wget='wget -c'

## Curl-related ##
alias headers='curl -I'
# Use JSON header with curl
alias curlj='curl -H "Content-Type: application/json"'
