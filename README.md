### tmux & neovim: the perfect match ##

- Install [tmux](https://github.com/tmux/tmux/wiki) from source otherwise you'll get an older version: https://gist.github.com/rschuman/6168833
- Install [neovim](https://neovim.io/) from source: https://github.com/neovim/neovim/wiki/Installing-Neovim#install-from-source
- Install the [ddu](https://github.com/Shougo/ddu.vim) plugin for displaying sources
- Install the [ddc](https://github.com/Shougo/ddc.vim) plugin for autocomplete based on sources (uses [TabNine](https://www.tabnine.com/))
- Install the [dein](https://github.com/Shougo/dein.vim) plugin for managing plugins
- Install the [falcon](https://github.com/fenetikm/falcon) color schemes
- Make sure you have ~/.tmux.conf
- Make sure you have ~/.vimrc



### other tweaks ###

- [ripgrep](https://github.com/BurntSushi/ripgrep) will speed up your life significantly
