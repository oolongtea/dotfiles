" --------------------------------------------------- "
" Manual stuff: deno, python3 virtualenv, dein install, ripgrep
"   https://docs.deno.com/runtime/manual/getting_started/installation
"   https://github.com/pyenv/pyenv#getting-pyenv
"   https://github.com/Shougo/dein-installer.vim
"   https://github.com/BurntSushi/ripgrep#installation
"   update pyenv python location
" Neovim-specific settings
if has('nvim')
    set guicursor=

    " specify python 3 settings
    let g:python_host_prog = '~/.pyenv/shims/python3.10'
    let g:python3_host_prog = '~/.pyenv/versions/3.10.13/envs/neovim-env/bin/python'

    if &compatible
      set nocompatible
    endif

    " Set Dein base path (required)
    let s:dein_base = '/home/obedespina/.cache/dein'

    " Set Dein source path (required)
    let s:dein_src = '/home/obedespina/.cache/dein/repos/github.com/Shougo/dein.vim'

    " Set Dein runtime path (required)
    execute 'set runtimepath+=' . s:dein_src

    if dein#load_state('~/.cache/dein')
      call dein#begin('~/.cache/dein')

      " Let dein manage dein
      call dein#add('~/.cache/dein/repos/github.com/Shougo/dein.vim')

      " Don't know why, but it's in the dein examples
      if !has('nvim')
        call dein#add('roxma/nvim-yarp')
        call dein#add('roxma/vim-hug-neovim-rpc')
      endif

      " Sane terminal-based colorscheme
      call dein#add('fenetikm/falcon')

      " Ecosystem for Neovim/Vim plugin development in Deno
      " Deno is a modern runtime for JavaScript and TypeScript
      call dein#add('vim-denops/denops.vim')

      " Dark deno-powered UI framework for neovim/Vim8
      call dein#add('Shougo/ddu.vim')
      call dein#add('Shougo/ddu-ui-ff')
      call dein#add('Shougo/ddu-source-file_rec')
      call dein#add('Shougo/ddu-kind-file')
      call dein#add('Shougo/ddu-filter-matcher_substring')
      call dein#add('Shougo/ddu-source-file')
      call dein#add('shun/ddu-source-buffer')
      call dein#add('Shougo/ddu-commands.vim')
      call dein#add('Shougo/ddu-source-line')
      call dein#add('shun/ddu-source-rg')
      call dein#add('matsui54/ddu-source-file_external')
      call dein#add('Shougo/ddu-source-file_point')
      call dein#add('Shougo/ddu-source-file_old')
      call dein#add('Shougo/ddu-source-action')
      call dein#add('Shougo/ddu-ui-filer')

      " Dark deno-powered completion framework for neovim/Vim8
      call dein#add('Shougo/ddc.vim')
      call dein#add('Shougo/ddc-around')
      call dein#add('Shougo/ddc-matcher_head')
      call dein#add('Shougo/ddc-sorter_rank')
      call dein#add('LumaKernel/ddc-tabnine')

      call dein#end()
      call dein#save_state()
    endif
    " ddc.vim configuration -------------------------------------------- "
    " Customize global settings
    " Use around source.
    " https://github.com/Shougo/ddc-around
    call ddc#custom#patch_global('sources', ['around'])

    " Use matcher_head and sorter_rank.
    " https://github.com/Shougo/ddc-matcher_head
    " https://github.com/Shougo/ddc-sorter_rank
    call ddc#custom#patch_global('sourceOptions', {
          \ '_': {
          \   'matchers': ['matcher_head'],
          \   'sorters': ['sorter_rank']},
          \ })

    " Change source options
    call ddc#custom#patch_global('sourceOptions', {
          \ 'around': {'mark': 'A'},
          \ })
    call ddc#custom#patch_global('sourceParams', {
          \ 'around': {'maxSize': 500},
          \ })

    " Customize settings on a filetype
    call ddc#custom#patch_filetype(['c', 'cpp'], 'sources', ['around', 'clangd'])
    call ddc#custom#patch_filetype(['c', 'cpp'], 'sourceOptions', {
          \ 'clangd': {'mark': 'C'},
          \ })
    call ddc#custom#patch_filetype('markdown', 'sourceParams', {
          \ 'around': {'maxSize': 100},
          \ })

    " TabNine -> ddc
    " \   'maxCandidates': 5,
    call ddc#custom#patch_global('sources', ['tabnine'])
    call ddc#custom#patch_global('sourceOptions', {
        \ 'tabnine': {
        \   'mark': 'TN',
        \   'isVolatile': v:true,
        \ }})

    " Mappings

    " <TAB>: completion.
    inoremap <silent><expr> <TAB>
    \ ddc#map#pum_visible() ? '<C-n>' :
    \ (col('.') <= 1 <Bar><Bar> getline('.')[col('.') - 2] =~# '\s') ?
    \ '<TAB>' : ddc#map#manual_complete()

    " <S-TAB>: completion back.
    inoremap <expr><S-TAB>  ddc#map#pum_visible() ? '<C-p>' : '<C-h>'

    " Use ddc.
    call ddc#enable()
    " ddc.vim configuration -------------------------------------------- "

    " ddu.vim configuration ----------------------------------------------- "
    " https://github.com/Shougo/shougo-s-github/blob/a8d5e269866d849f7e83a0e6744a8c4207dcc588/vim/rc/ddc.toml
	" Define mappings
	autocmd FileType ddu-ff call s:ddu_ff_my_settings()
	function! s:ddu_ff_my_settings() abort
	  nnoremap <buffer> <CR>
	  \ <Cmd>call ddu#ui#ff#do_action('itemAction')<CR>
	  nnoremap <buffer> <Space>
	  \ <Cmd>call ddu#ui#ff#do_action('toggleSelectItem')<CR>
	  nnoremap <buffer> i
	  \ <Cmd>call ddu#ui#ff#do_action('openFilterWindow')<CR>
	  nnoremap <buffer> q
	  \ <Cmd>call ddu#ui#ff#do_action('quit')<CR>
	endfunction

	autocmd FileType ddu-ff-filter call s:ddu_filter_my_settings()
	function! s:ddu_filter_my_settings() abort
	  inoremap <buffer> <CR>
	  \ <Esc><Cmd>call ddu#ui#ff#close()<CR>
	  nnoremap <buffer> <CR>
	  \ <Cmd><Cmd>call ddu#ui#ff#close()<CR>
	endfunction
    " ddu.vim configuration ----------------------------------------------- "
    call ddu#custom#alias('source', 'file_rg', 'file_external')
    call ddu#custom#patch_global({
        \   'ui': 'ff',
        \   'sourceOptions': {
        \     '_': {
        \       'ignoreCase': v:true,
        \       'matchers': ['matcher_substring'],
        \     },
        \     'file_old': {
        \       'matchers': [
        \         'matcher_substring', 'matcher_relative', 'matcher_hidden',
        \       ],
        \     },
        \     'file_external': {
        \       'matchers': [
        \         'matcher_substring', 'matcher_hidden',
        \       ],
        \     },
        \     'file_rec': {
        \       'matchers': [
        \         'matcher_substring', 'matcher_hidden',
        \       ],
        \     },
        \     'file': {
        \       'matchers': [
        \         'matcher_substring', 'matcher_hidden',
        \       ],
        \       'sorters': ['sorter_alpha'],
        \     },
        \     'dein': {
        \       'defaultAction': 'cd',
        \     },
        \   },
        \   'sourceParams': {
        \     'file_external': {
        \       'cmd': ['git', 'ls-files', '-co', '--exclude-standard'],
        \     },
        \   },
        \   'uiOptions': {
        \     'filer': {
        \       'toggle': v:true,
        \     },
        \   },
        \   'uiParams': {
        \     'ff': {
        \       'filterSplitDirection': 'floating',
        \       'previewFloating': v:true,
        \     },
        \     'filer': {
        \       'split': 'no',
        \       'toggle': v:true,
        \     },
        \   },
        \   'kindOptions': {
        \     'file': {
        \       'defaultAction': 'open',
        \     },
        \     'word': {
        \       'defaultAction': 'append',
        \     },
        \     'deol': {
        \       'defaultAction': 'switch',
        \     },
        \     'action': {
        \       'defaultAction': 'do',
        \     },
        \     'readme_viewer': {
        \       'defaultAction': 'open',
        \     },
        \   },
        \   'actionOptions': {
        \     'narrow': {
        \       'quit': v:false,
        \     },
        \   },
        \ })

    call ddu#custom#patch_local('files', {
        \   'uiParams': {
        \     'ff': {
        \       'split': has('nvim') ? 'floating' : 'horizontal',
        \     }
        \   },
        \ })

    call ddu#custom#patch_global({
        \   'sourceParams': {
        \     'file_rg': {
        \       'cmd': ['rg', '--files', '--glob', '!.git',
        \               '--color', 'never', '--no-messages'],
        \       'updateItems': 50000,
        \     },
        \     'rg': {
        \       'args': [
        \         '--ignore-case', '--column', '--no-heading', '--color', 'never',
        \       ],
        \     },
        \   }
        \ })

    call ddu#custom#patch_global({
        \   'filterParams': {
        \     'matcher_substring': {
        \       'highlightMatched': 'Search',
        \     },
        \   }
        \ })

    call ddu#custom#action('kind', 'file', 'grep',
        \ { args -> GrepAction(args) })
    function! GrepAction(args)
      " NOTE: param "path" must be one directory
      let path = a:args.items[0].action.path
      let directory = isdirectory(path) ? path : fnamemodify(path, ':h')
      call ddu#start({
          \ "name": a:args.options.name,
          \ "push": v:true,
          \ "sources": [
          \   {
          \     "name": "rg",
          \     "params": {
          \       "path": path,
          \       "input": input('Pattern: '),
          \     },
          \   },
          \ ],
          \ })
    endfunction

    " === ddu shorcuts === "
    "   <space>b  - Browser currently open buffers
    "   <leader>r - Browse list of files in current directory
    "   <leader>/ - Search current directory for occurences of given term and
    "   close window if no results
    "   <leader>w - Search current directory for occurences of word under cursor
    nnoremap <space>b
    \ <Cmd>Ddu buffer
    \ -ui-param-startFilter=v:false<CR>

    nnoremap <space>r
    \ <Cmd>Ddu file_rec
    \ -source-option-path=.
    \ -ui-param-split=floating
    \ -ui-param-winRow=1
    \ -ui-param-startFilter=v:true<CR>

    nnoremap <space>/
    \ <Cmd>Ddu
    \ -name=search rg
    \ -resume=v:false
    \ -ui-param-ignoreEmpty
    \ -source-param-input=`input('Pattern: ')`<CR>

    nnoremap <space>w
    \ <Cmd>Ddu
    \ -name=search line
    \ -resume=v:false
    \ -input=`expand('<cword>')`
    \ -ui-param-startFilter=v:false<CR>

    " ddu.vim configuration ----------------------------------------------- "

    set termguicolors
    let g:falcon_background = 0
    let g:falcon_inactive = 1
    colorscheme falcon

endif
" end of Neovim-specific settings
" --------------------------------------------------- "
" Normal Vim-specific settings

syntax on
filetype plugin indent on
syntax enable

set background=dark

" Remap <Leader> to ,
let mapleader = ","

" View line numbers
set number
set numberwidth=1
set relativenumber

" Change line number color
hi CursorLineNr ctermfg=grey guifg=grey ctermbg=black guibg=black
hi LineNr ctermfg=grey guifg=grey ctermbg=black guibg=black

" size of hard tabstop
set tabstop=4

" size of an "indent"
set shiftwidth=4

" disable mouse
set mouse=

" make "tab" insert indents instead of tabs at beginning of a line
set smarttab

" expand "tab" to spaces
set expandtab

" highlight search pattern matches
set hlsearch

" highlight lines that are longer than 80 columns
set colorcolumn=81
highlight ColorColumn ctermbg=0

" map kj and jk to <Esc>
:inoremap jk <Esc>
:inoremap kj <Esc>

" flip mapping between colon (:) and semicolon (;)
nnoremap ; :
nnoremap : ;
vnoremap ; :
vnoremap : ;

" TODO: handle if rg is not installed
" Use grep.
" set grepprg=internal
set grepprg=rg\ --vimgrep

" https://github.com/Shougo/shougo-s-github/blob/master/vim/rc/
" Highlight <>.
set matchpairs+=<:>

" Visual mode keymappings:
" Indent
nnoremap > >>
nnoremap < <<
xnoremap > >gv
xnoremap < <gv

function! AddNumbersFunc(num) abort
  let prev_line = getline('.')[: col('.')-1]
  let next_line = getline('.')[col('.') :]
  let prev_num = matchstr(prev_line, '\d\+$')
  if prev_num != ''
    let next_num = matchstr(next_line, '^\d\+')
    let new_line = prev_line[: -len(prev_num)-1] .
          \ printf('%0'.len(prev_num . next_num).'d',
          \    max([0, substitute(prev_num . next_num, '^0\+', '', '')
          \         + a:num])) . next_line[len(next_num):]
  else
    let new_line = prev_line . substitute(next_line, '\d\+',
          \ "\\=printf('%0'.len(submatch(0)).'d',
          \         max([0, substitute(submatch(0), '^0\+', '', '')
          \              + a:num]))", '')
  endif

  if getline('.') !=# new_line
    call setline('.', new_line)
  endif
endfunction

" Improved increment.
nmap <C-i> <SID>(increment)
nmap <C-o> <SID>(decrement)
nnoremap <silent> <SID>(increment)    :AddNumbers 1<CR>
nnoremap <silent> <SID>(decrement)   :AddNumbers -1<CR>
command! -range -nargs=1 AddNumbers
      \ call AddNumbersFunc((<line2>-<line1>+1) * eval(<args>))
" ------------------------------------------------------------------------------

" highlight trailing whitespace
highlight ExtraWhitespace ctermbg=red guibg=red
match ExtraWhitespace /\s\+$/
autocmd BufWinEnter * match ExtraWhitespace /\s\+$/
autocmd InsertEnter * match ExtraWhitespace /\s\+\%#\@<!$/
autocmd InsertLeave * match ExtraWhitespace /\s\+$/
autocmd BufWinLeave * call clearmatches()

" make bash interactive so that it loads ~/.bash_aliases
" useful for when running jobstart() commands
" set shellcmdflag=-ic


" quickly open a new tab
nnoremap <leader>t :tabnew<CR>


" Auto-commands
" augroup autosourcing
"     if(argc() == 0)
"         au VimEnter * nested :call LoadSession() " Uncomment to automatically load session
"         au VimLeave * :call MakeSession()
"     endif
" augroup END

" shortcuts for X11 clipboard access
:vnoremap <leader>y "+y
:vnoremap <leader>p "+p

" clear trailing whitespace
" %s/\s\+$//e

" vim-javacomplete2
" autocmd FileType java setlocal omnifunc=javacomplete#Complete


" Do not display the greetings message at the time of Vim start.
set shortmess=aTI

"Performance improvements

" https://www.reddit.com/r/vim/comments/8m0632/what_performance_related_things_do_you_have_in/
set synmaxcol=200 "Don't bother highlighting anything over 200 chars
let did_install_default_menus = 1 "No point loading gvim menu stuff
" let loaded_matchparen = 1 "highlighting matching pairs so slow
set lazyredraw

" https://stackoverflow.com/a/17189261
" :syntax sync fromstart

" https://www.youtube.com/watch?v=XA2WjJbmmoM
" search down into subfolders
set path+=**
" display all matching files when we tab complete
set wildmenu
" Create 'tags' file
command! MakeTags !ctags -R .
" ^] to jump to tag under cursor
" g^] for ambiguous tag
" Tweaks for file browsing
" see the help for |netrw-browse-maps|
let g:netrw_banner=0  " disable annoying banner
let g:netrw_browse_split=4 " open in prior window
let g:netrw_altv=1 " open splits to the right
let g:netrw_liststyle=3 " tree view
" FIXME: some issue with the below when redirecting output to vim
" let g:netrw_list_hide=netrw_gitignore#Hide()
" let g:netrw_list_hide.=',\(*\+\s\s\)\zs\.\S\+)'

" SNIPPETS

" mapping to insert ipdb
" http://stackoverflow.com/a/6547911
nnoremap <Leader>p :call InsertLine()<CR>
" nnoremap <leader>p :-1read $HOME/.vim/.skeletons/ipdb<CR>

function! InsertLine()
  let trace = expand("import ipdb; ipdb.set_trace()")
  execute "normal o".trace
endfunction

nnoremap <Leader>m :call InsertMain()<CR>
" nnoremap <leader>m :-1read $HOME/.vim/.skeletons/pythonmain<CR>

function! InsertMain()
  let trace = expand("if __name__ == '__main__':\nmain()")
  execute "normal o".trace
endfunction

" from zc, the Vim Man with the Plan
function! OTest()
    " filename without extension
    let filename = expand('%:t:r')
    execute 'find ' . filename . 'Test.py'
endfunction

command! OpenTest :call OTest()

" from nh
autocmd FileType python nnoremap <silent> K :silent !python3 -c 'help("<cword>")'<CR>:redraw!<CR>
autocmd FileType python nnoremap <silent> <leader><CR> :silent term ++close python3 -i %<CR>

" https://gist.github.com/romainl/047aca21e338df7ccf771f96858edb86
" make list-like commands more intuitive
function! CCR()
    let cmdline = getcmdline()
    if cmdline =~ '\v\C^(ls|files|buffers)'
        " like :ls but prompts for a buffer command
        return "\<CR>:b"
    elseif cmdline =~ '\v\C/(#|nu|num|numb|numbe|number)$'
        " like :g//# but prompts for a command
        return "\<CR>:"
    elseif cmdline =~ '\v\C^(dli|il)'
        " like :dlist or :ilist but prompts for a count for :djump or :ijump
        return "\<CR>:" . cmdline[0] . "j  " . split(cmdline, " ")[1] . "\<S-Left>\<Left>"
    elseif cmdline =~ '\v\C^(cli|lli)'
        " like :clist or :llist but prompts for an error/location number
        return "\<CR>:sil " . repeat(cmdline[0], 2) . "\<Space>"
    elseif cmdline =~ '\C^old'
        " like :oldfiles but prompts for an old file to edit
        set nomore
        return "\<CR>:sil se more|e #<"
    elseif cmdline =~ '\C^changes'
        " like :changes but prompts for a change to jump to
        set nomore
        return "\<CR>:sil se more|norm! g;\<S-Left>"
    elseif cmdline =~ '\C^ju'
        " like :jumps but prompts for a position to jump to
        set nomore
        return "\<CR>:sil se more|norm! \<C-o>\<S-Left>"
    elseif cmdline =~ '\C^marks'
        " like :marks but prompts for a mark to jump to
        return "\<CR>:norm! `"
    elseif cmdline =~ '\C^undol'
        " like :undolist but prompts for a change to undo
        return "\<CR>:u "
    else
        return "\<CR>"
    endif
endfunction
cnoremap <expr> <CR> CCR()
